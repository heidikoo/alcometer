import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  weight: number;
  gender: any;
  time: number;
  bottles: number;
  result: number;

  constructor() {}
    calculate() {
      let grams;
      let litres;
      let burning;
      let left;

      litres = this.bottles * 0.33;
      grams = litres * 8 * 4.5;
      burning = this.weight / 10;
      left = grams - (burning * this.time);
      console.log(this.gender);
      if (this.gender === "m") {
       this.result = left / (this.weight * 0.7);
      }
       else {
        this.result = left / (this.weight * 0.6);
      }

    
    }
}
